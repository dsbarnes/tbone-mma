const folder = document.getElementById('top-secret-folder');


// Get a random int between two values:
const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  // The maximum is exclusive and the minimum is inclusive
  return Math.floor(Math.random() * (max - min) + min); 
}


const newFolderPosition = () => {
  const newTopPosition = getRandomInt(173, 745);
  folder.style.top = newTopPosition + "px";

  const newLeftPosition = getRandomInt(81, window.innerWidth - 169);
  folder.style.left = newLeftPosition + "px";
}


folder.addEventListener('mouseenter', () => { newFolderPosition(); })
folder.addEventListener('click',      () => { newFolderPosition(); })