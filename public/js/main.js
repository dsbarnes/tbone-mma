const contactButton = document.getElementById('nav-contact')
const mobileNav = document.getElementById('mobile-nav')
const navImg = document.getElementById('nav-img')

const toggleMenu = () => {
    console.log("clicked")
    if (mobileNav.style.display == "" ||
        mobileNav.style.display === "none") {

        mobileNav.style.display = "block"
        mobileNav.style.zIndex= "2"
    }
    else{
        mobileNav.style.display = "none"
        mobileNav.style.zIndex= "-1"
    }
}

contactButton.addEventListener('click', toggleMenu)

window.addEventListener('resize', () => {
    if(window.innerWidth <= 865){
        navImg.addEventListener('click', toggleMenu)
    }

    if(window.innerWidth > 865){
        mobileNav.style.display = "none"
        navImg.removeEventListener('click', toggleMenu)
    }
});

// If the window loads before being resized
// the event handle still needs added
if(window.innerWidth <= 865){
    navImg.addEventListener('click', toggleMenu)
    console.log("small window")
}